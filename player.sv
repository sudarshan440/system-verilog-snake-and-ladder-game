class player;
  int player1,player2,min,max;
  dice d;
  
  
  function new(int min,int max);
    this.min = min;
    this.max=max;
    //d = new(min,max);
    d = new();
  endfunction :new
  
  
  function int player1chance();
    player1=d.throw();
    return player1;
  endfunction : player1chance
  
  function int player2chance();
    player2=d.throw();
    return player2;
  endfunction : player2chance
  
endclass : player